/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Nelson Chen <nelsonc1>
 */

import org.newdawn.slick.SlickException;

public class Sword extends Item {
	
	public static final int DAMAGE_INCREASE = 30;
	private int newMaxDamage;
	
	/** Creates item Sword Of Strength. 
	 * @param image_path The image file location.
	 * @param xPos The Sword's x location in pixels.
	 * @param yPos The Sword's y location in pixels.
	 * @throws SlickException
	 */
	public Sword(String image_path, double xPos, double yPos)
	throws SlickException {
		super(image_path, xPos, yPos);
	}

	/**
	 * @param player The player unit of the game.
	 */
	public void take(Player player) {
		
		newMaxDamage = player.getmaxDamage() + DAMAGE_INCREASE;
		player.setmaxDamage(newMaxDamage);
		
		setTaken(true);
		player.getInventory().add(this);
	}

}
