/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Author: Nelson Chen <nelsonc1>
 */

import org.newdawn.slick.SlickException;

public class Zombie extends AggresiveMonster {

	/** Creates a zombie within the game world.
	 * @param xPos The initial x coordinate of the zombie.
	 * @param yPos The initial y coordinate of the zombie.
	 * @throws SlickException
	 */
	public Zombie(String image_path, double xPos, double yPos) 
	throws SlickException 
	{
		super(image_path, xPos, yPos, 60, "Zombie", 800, 10);
	}

	/** Updates the status of a zombie.
	 * @param delta Time passed since last frame (milliseconds)
	 * @param world The world the zombie is in.
	 * @throws SlickException
	 */
	public void update(int delta, World world)
	throws SlickException
	{
		super.update(delta, world);
	}
}
