/* SWEN20003 Object Oriented Software Development
 * RPG Game Engine
 * Sample Solution
 * Author: Matt Giuca <mgiuca>
 * Editor: Nelson Chen <nelsonc1>
 */

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import java.util.ArrayList;

/** Represents the entire game world.
 * (Designed to be instantiated just once for the whole game).
 */
public class World
{
    public static final int PLAYER_START_X = 756, PLAYER_START_Y = 684;
    
    // Initialize in game objects
    private Player player = null;
    private TiledMap map = null;
    private Camera camera = null;
    private Item elixir, sword, tome, amulet;
    private ArrayList<NPC> npcList = null;
    private ArrayList<Monster> monsterList = null;
    private ArrayList<Monster> deadList = null;    
    public ArrayList<Item> itemList = null;
 
    /** Map width, in pixels. 
     * @return map width in pixels.
     */
    public int getMapWidth(){
        return map.getWidth() * getTileWidth();
    }

    /** Map height, in pixels. 
     * @return map height in pixels.
     */
    public int getMapHeight()
    {
        return map.getHeight() * getTileHeight();
    }

    /** Tile width, in pixels. 
     * @return tile width of map in pixels.
     */
    public int getTileWidth(){
        return map.getTileWidth();
    }

    /** Tile height, in pixels. 
     * @return tile height of map in pixels. 
     */
    public int getTileHeight(){
        return map.getTileHeight();
    }

    /** Returns player of the game.
     * @return player of the game.
     */
    public Player getPlayer(){
    	return player;
    }
    
    /** Returns item elixir of life. 
     * @return the elixir.
     */
    public Item getElixir(){
    	return elixir;
    }
    
    /** Returns item sword of strength. 
     * @return the sword.
     */
    public Item getSword(){
    	return sword;
    }
    
    /** Returns item tome of agility.
     * @ return the tome.
     */
    public Item getTome(){
    	return tome;
    }
    
    /** Returns item amulet of vitality.
     * @return the amulet.
     */
    public Item getAmulet(){
    	return amulet;
    }
    
    /** Returns a list of NPCs. 
     * @return NPC list.
     */
    public ArrayList<NPC> getNPCList(){
    	return npcList;
    }
    
    /** Returns a list of the monsters.
     * @return Monster List.
     */
	public ArrayList<Monster> getMonsterList() {
		return monsterList;
	}
	
	/** Returns a list of dead monsters.
	 * @return Dead Monster List;
	 */
	public ArrayList<Monster> getDeadList(){
		return deadList;
	}
	
	/** Returns a list of items.
	 * @return Item List.
	 */
	public ArrayList<Item> getItemList(){
		return itemList;
	}
	
    /** Create a new World object. 
     * @throws SlickException
     */
    public World()
    throws SlickException
    {
        map = new TiledMap(RPG.ASSETS_PATH + "/map.tmx", RPG.ASSETS_PATH);
        player = new Player(RPG.ASSETS_PATH + "/units/player.png", PLAYER_START_X, PLAYER_START_Y);
        camera = new Camera(player, RPG.SCREEN_WIDTH, RPG.SCREEN_HEIGHT);
    
        initialiseNPC();
        initialiseMonsters();
        initialiseItem();
    }
    
    /** Render the entire screen, so it reflects the current game state.
     * @param g The Slick graphics object, used for drawing.
     * @throws SlickException
     */
    public void render(Graphics g)
    throws SlickException
    {
        // Render the relevant section of the map
        int x = -(camera.getMinX() % getTileWidth());
        int y = -(camera.getMinY() % getTileHeight());
        int sx = camera.getMinX() / getTileWidth();
        int sy = camera.getMinY()/ getTileHeight();
        int w = (camera.getMaxX() / getTileWidth()) - (camera.getMinX() / getTileWidth()) + 1;
        int h = (camera.getMaxY() / getTileHeight()) - (camera.getMinY() / getTileHeight()) + 1;
        map.render(x, y, sx, sy, w, h);
        player.renderPanel(g);       
        
        // Translate the Graphics object
        g.translate(-camera.getMinX(), -camera.getMinY());

        // Render units
        renderUnit(g, camera);
        
        // Render the player
        player.render();
        
    }

    /** Update the game state for a frame.
     * @param dir_x The player's movement in the x axis (-1, 0 or 1).
     * @param dir_y The player's movement in the y axis (-1, 0 or 1).
     * @param delta Time passed since last frame (milliseconds).
     * @param key_z The key to attack monsters.
     * @param key_x The key to pick up items.
     * @param key_n The key to interact with NPCs.
     * @throws SlickException
     */
    public void update(int dir_x, int dir_y, int delta, int key_z, int key_x ,int key_n)
    throws SlickException
    {
        player.update(this, dir_x, dir_y, delta, key_z, key_x ,key_n);
        camera.update();
        
        for (NPC n: npcList){
        	n.update(delta, this);
        }
        
        for (Monster m: monsterList){
        	m.update(delta, this);
        }
        
        for (Monster m : deadList){
        	monsterList.remove(m);
        }
        
        deadList.clear();
    }

    /** Determines whether a particular map coordinate blocks movement.
     * @param x The map x coordinate (in pixels).
     * @param y The map y coordinate (in pixels).
     * @return True if the coordinate blocks movement.
     */
    public boolean terrainBlocks(double x, double y)
    {
        // Check for the bound of map
        if (x < 0 || y < 0 || x > getMapWidth() || y > getMapHeight()) {
            return true;
        }
        
        // Check the tile properties
        int tile_x = (int) x / getTileWidth();
        int tile_y = (int) y / getTileHeight();
        int tileid = map.getTileId(tile_x, tile_y, 0);
        String block = map.getTileProperty(tileid, "block", "0");
        return !block.equals("0");
    }
    
    /** Renders all units of the game.
     * @param g The current Slick graphics context.
     * @param camera The camera object.
     */
    public void renderUnit(Graphics g, Camera camera)
    {
    	// Render NPCs
    	for (NPC n : npcList){
    		n.render(g, camera);
    	}
        
    	// Render monsters
        for (Monster m : monsterList){
        	m.render(g, camera);
        }
        
        // Render items
        for (Item i : itemList){
        	i.render(g, camera);
        }
    }

    /** Initiliase NPCs on map 
     * @throws SlickException
     */
    public void initialiseNPC()
    throws SlickException
    {
    	npcList = new ArrayList<NPC>(3);
    	npcList.add(new Prince(RPG.ASSETS_PATH + "/units/prince.png", 467, 679));
        npcList.add(new Elvira(RPG.ASSETS_PATH + "/units/shaman.png", 738, 549));
        npcList.add(new Garth(RPG.ASSETS_PATH + "/units/peasant.png", 756, 870));
    }
    
    /** Initiliase items on map 
     * @throws SlickException
     */
    public void initialiseItem()
    throws SlickException
    {
    	itemList = new ArrayList<Item>(4);
    	itemList.add(elixir = new Elixir(RPG.ASSETS_PATH + "/items/elixir.png", 1976,402));
    	itemList.add(sword = new Sword(RPG.ASSETS_PATH + "/items/sword.png", 546, 6707));
    	itemList.add(amulet = new Amulet(RPG.ASSETS_PATH + "/items/amulet.png", 965, 3565));
    	itemList.add(tome = new Tome(RPG.ASSETS_PATH + "/items/book.png", 4791, 1253));
    }
    
    /** Initiliase monsters on map 
     * @throws SlickException
     */
    public void initialiseMonsters()
    throws SlickException
    {
    	monsterList = new ArrayList<Monster>();
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1431,864)) ;
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1154,1321));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 807,2315));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 833,2657));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1090,3200));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 676,3187));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 836,3966));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 653,4367));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1343,2731));	
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1835,3017));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1657,3954));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1054,5337));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 801,5921));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 560,6682));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1275,5696));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 1671,5991));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 2248,6298));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 2952,6373));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 3864,6695));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 4554,6443));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 4683,6228));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 5312,6606));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 5484,5946));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 6371,5634));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 5473,3544));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 5944,3339));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 6301,3414));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 6388,1994));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 6410,1584));
    	monsterList.add(new GiantBat(RPG.ASSETS_PATH + "/units/dreadbat.png", 5315,274));
    	
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 681,3201));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 691,4360));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2166,2650));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2122,2725));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2284,2962));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2072,4515));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2006,4629));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2385,4629));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2446,4590));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2517,4532));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 4157,6730));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 4247,6620));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 4137,6519));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 4234,6449));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5879,4994));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5954,4928));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 6016,4866));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5860,4277));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5772,4277));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5715,4277));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5653,4277));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5787,797));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5668,720));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5813,454));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5236,917));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 5048,1062));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 4845,996));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 4496,575));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 3457,273));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 3506,779));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 3624,1192));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2931,1396));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2715,1326));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2442,1374));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2579,1159));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2799,1269));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2768,739));
    	monsterList.add(new Zombie(RPG.ASSETS_PATH + "/units/zombie.png", 2099,956));
    	
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 1889,2581));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 4502,6283));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 5248,6581));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 5345,6678));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 5940,3412));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 6335,3459));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 6669,260));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 6598,339));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 6598,528));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 6435,528));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 6435,678));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 5076,1082));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 5191,1187));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 4940,1175));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 4760,1039));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 4883,889));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 4427,553));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3559,162));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3779,1553));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3573,1553));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3534,2464));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3635,2464));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3402,2861));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3151,2857));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 3005,2997));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2763,2263));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2638,2263));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2907,1270));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2331,598));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2987,394));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 1979,394));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2045,693));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2069,1028));
    	monsterList.add(new Bandit(RPG.ASSETS_PATH + "/units/bandit.png", 2621,1337));
    	
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 1255,2924));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2545,4708));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 4189,6585));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 5720,622));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 5649,767));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 5291,312));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 5256,852));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 4790,976));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 4648,401));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 3628,1181));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 3771,1181));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 3182,2892));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 3116,3033));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2803,2901));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2850,2426));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2005,1524));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2132,1427));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2242,1343));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2428,771));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2427,907));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2770,613));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2915,477));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 1970,553));
    	monsterList.add(new Skeleton(RPG.ASSETS_PATH + "/units/skeleton.png", 2143,1048));
    	
    	monsterList.add(new Draelic(RPG.ASSETS_PATH + "/units/necromancer.png", 2069,510));
    
    	deadList = new ArrayList<Monster>();
    }
}
